-- Run MySQL and Apache in XAMPP
-- Open terminal/Command line and run the following command:
-- "mysql -u root" for windows
-- "mysql -u root"
--commands are not case sensitive
--List down the databases inside the DBMS
SHOW DATABASES;
NOTE - SEMICOLON is important since it signifies
end of command in mysql --Create a database
CREATE DATABASE music_db;
-- Remove database
DROP DATABASE music_db;
--Select database
USE music_db;
--Create Tables
--Table columns have the folling format: [column_name] [data_type] [other_options]
--INT - specifies that the column should accept integer data
--  VARCHAR(character_limit) - specifies that the column should accept string data, the limit is to avoid data bloating
-- DATE - refers to the format of YYYY-MM-DD
-- TIME - refers to the format of HH:MM:SS
-- DATETIME - refers to the format of YYYY-MM-DD HH:MM:SS
-- NOT NULL means it is required
-- AUTO_INCREMENT means the value will be automatically incremented by 1
-- PRIMARY KEY (column_name ) specifies that it must be unique identifier for the each row in the table and cannot be null
CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);
CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);
-- CONSTRAINT  is a command rule, this is optional, in this context we used it to identify the foreign key. This allows you to specify a condition that must be met for the data to be inserted into the table
-- FOREIGN KEY is a column that references the primary key of another table. It is the connection between tables
-- REFERENCE refers to the table and column that the foreign key is referencing. Where we get the FK value
-- ON UPDATE CASCADE - everytime the parent table updates, it should also affect the related table.
--ON DELETE RESTRICT - the tables cannot be immediately deleted when there are more tables related to it.
CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id FOREIGN KEY (artist_id) REFERENCES artists(id) ON UPDATE CASCADE ON DELETE RESTRICT
);
/* 
 Miniactivity
 1. Create a table for songs.
 2. Put a required auto increment id.
 3. Decalare a song name with 50 char limit, this should be required.
 4. Delcare a song length with the data type time and this should be required.
 5. Declare a genre with 50 char limit and this should be required.
 6. Declare an integer as album id that should be required.
 7. Create a primary key referring to the id of the songs
 8. Create a foreign key and name it fk_song_album_id
 8.1. This should be reffered to the album id
 8.2. It should have cascaded update and restricted delete
 9. Run create table songs
 
 
 */
CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    song_length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_songs_album_id FOREIGN KEY (album_id) REFERENCES albums(id) ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    date_time_created DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_playlists_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE TABLE playlists_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_playlists_songs_playlist_id FOREIGN KEY (playlist_id) REFERENCES playlists(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id FOREIGN KEY (song_id) REFERENCES songs(id) ON UPDATE CASCADE ON DELETE RESTRICT
)